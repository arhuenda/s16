// Create a variable number that will store the value of the number provided by the user via prompt
var num1 = parseInt(prompt("Please enter the number:"));

// Create a for loop that will be initialized with the number provided by the user
for(num1; num1 !== 0; num1--){
	console.log(num1);

	// Create a condition that if the current value is less than or equal to 50
	if (num1 <= 50) {
		break;
	}

	// Create another condition that if the current value is divisible by 10
	if (num1 % 10 === 0) {
		console.log("The number " + num1 + " is divisible by 10. Skipping the number ");
		continue;
	} else if (num1 % 5 === 0) { 
		// Create another condition that if the current value is divisible by 5
		console.log(num1);
	}
}

// Create a variable that will containt the string
var string1 = "supercalifragilisticexpialidocious";

// Create another variable that will store the consonants
var consonants = [];

// Create another for loop that will iterate through the individual letters of the string
for(let x = 0; x < string1.length; x++){


	if (
		 string1[x].toLowerCase() == 'a' ||
		 string1[x].toLowerCase() == 'e' ||
		 string1[x].toLowerCase() == 'i' ||
		 string1[x].toLowerCase() == 'o' ||
		 string1[x].toLowerCase() == 'u'
	) {
		console.log(string1[x]);
		continue;
	} else {
		consonants.push(string1[x]);
	}

}

// Display all the consonants in an array
console.log(consonants);

